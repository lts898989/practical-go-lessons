# Chapter 3: 终端

![](imgs/3-terminal.c81a1c3d.jpg)

## 1 本章中能学到的内容?

- 什么是shell、bash、terminal？
- 如何运行一个终端应用程序？
- 如何使用 bash 发出基本命令？
- 如何改善 Windows 上的“开发人员体验”？

### 1.1 本章中提及的技术概念
- GUI:图形用户界面
- CLI:命令行界面
- Shell
- Bash
- WSL：适用于 Linux 的 Windows 子系统

## 2 介绍

Go 程序员的日常开发工作需要使用终端。本章将解释什么是终端以及如何使用它。如果你已经熟悉它，可以跳过这一章。

## 3 图形用户界面(GUI)

macOS、Windows 或 Linux 等操作系统提供丰富的图形用户界面(GUI)。要启动安装在您计算机上的程序，您通常双击桌面上的图标。程序将显示在具有交互式界面的窗口中：菜单、侧边栏、按钮...

我们说这些程序提供了图形用户界面(GUI)。绝大多数用户将使用提供这些接口的程序。图形界面易于使用且直观。

## 4 命令行界面(CLI)

图形用户界面并不总是存在。第一台计算机没有这样的能力。但是这些计算机的用户是如何设法启动和使用程序的呢？计算机附带了命令行界面。该接口也称为“shell”。shell 是一种可以向操作系统[^shell]。传递命令的程序。Shell 是指定此类程序的通用术语。最著名的 shell 是 bash(Bourne Again Shell)。Bash 默认随 macOS 和绝大多数 Linux 发行版一起提供。默认情况下，Windows 还附带一个 shell(但它不是 bash)。

[^shell]: http://www.catb.org/jargon/html/S/shell.html

## 5 如何与 shell 交互：终端

以前的计算机启动后，shell 直接呈现给用户。在现代计算机上，我们必须启动一个程序来与 shell 交互。该程序通常称为终端。我们将看到如何使用 MacOS、Linux (GNOME) 和 Windows 打开终端。

### 5.1 macOS

- 打开 Finder:

![Finder Icon](imgs/3-terminal.finder.png)

- 然后在菜单栏上，单击“Go”，然后单击“Utilities”

![Utilities](imgs/3-terminal.finder-bar.png)

- 将打开一个窗口。点击“终端”

![Terminal Application](imgs/3-terminal.window.png)

请注意，我使用的是自定义终端。因此，您可能会看到不同的输出；这是完全正常的。

### 5.2 Linux(Ubuntu)

- 在 Ubuntu 上，您可以使用快捷键 Ctrl+Alt+T。
- 您还可以使用 Ubuntu Dash 启动终端。输入“终端”，应用程序就会出现。

### 5.3 Windows

- 单击开始按钮，然后在文本框中键入“**cmd**”(用于命令提示符)。

![Search for cmd application](imgs/3-terminal.window-cmd.png)

- 然后点击应用程序 **cmd**。

![Open cmd application on Windows](imgs/3-terminal.window-cmd2.png)

- 应该会出现一个黑色窗口；这是你的终端！

![Windows terminal](imgs/3-terminal.window-terminal.png)

#### 5.3.1 cmder

终端和 Windows 外壳在今天的基础上不是很实用。我建议您安装 cmder[^cmder] 以使您的开发人员在 Windows 上的生活更轻松。Cmder 是一个模拟器，允许您使用 Linux/MacOS 上可用的命令。安装过程很简单(在 GitHub 上下载最新版本)然后启动安装向导。

[^cmder]: https://cmder.net/ , github repository : https://github.com/cmderdev/cmder

安装cmder后，启动程序“Cmder”打开你的全新终端。

#### 5.3.2 适用于 Windows 的 Bash

默认情况下，您不能在 Windows 计算机上使用 bash。这不是问题，但这意味着您必须为每个 macOS/Linux 命令找到等效的 Windows命令。它在某些时候可能很麻烦，因为网络上的许多示例和教程并不总是为 Windows 提供等效的命令。

微软宣布您现在可以在您的 Windows 计算机上安装“Windows 子系统 Linux”(WSL)。这是一个好消息，因为您将使用 bash。您可以在 Microsoft 网站上找到安装说明： [https://docs.microsoft.com/en-us/windows/wsl/install-win10](https://docs.microsoft.com/en-us/windows/wsl/install-win10) (适用于 Windows 10)。

我强烈建议您安装它，因为即使我在接下来的部分中尝试为 Windows 提供等效的基本命令，它也会使您的开发环节更轻松。

## 6 如何使用终端

打开终端后，您将看到一个黑色窗口。这是您可以键入命令的界面。这不直观，因为要键入命令；你必须先了解他们！每个命令都有一个名称。要启动命令，您将输入其名称，最后输入一些选项，然后按回车。让我们举个例子。

### 6.1 关于美元符号

在示例中，您将看到一条以美元符号开头的行。这是一个约定；它的意思是“将它输入到你的终端中”，当你想重现这些例子时，**不要输入美元**，只输入美元之后的所有内容。

### 6.2 MacOS / Linux

假设我们要列出桌面的内容。键入以下命令(用您的名字替换 “maximilienandile”字符串)。

- macOS

`$ ls /Users/maximilienandile/Desktop`

- Linux

`$ ls /home/maximilienandile/Desktop`

然后按回车。您将看到一个文件和目录列表。

现在输入以下命令：

`$ pwd`

按回车键。结果如下：

`/Users/maximilienandile`

这里我们使用了两个命令：

- **ls**：允许你列出目录的内容
- **pwd**: 允许您打印工作目录，(打印您所在目录的名称)

### 6.3 Windows(没有适配 Linux 的 Windows )

列出当前目录内容的命令是 **dir**:

![window terminal](imgs/3-terminal.window-terminal-dir.png)

每行将代表一个文件或一个目录。第三列有“<DIR>”，如果是目录。如果它是文件，则什么都没有。

### 6.4 Windows(安装了 WSL 和 Cmder的Windows)

如果您已经安装了 **WSL** 和 **cmder**，第一步是启动 cmder(例如，通过 Windows 菜单)。然后您可以通过键入以下命令来启动 **bash** ：

`$ bash`

然后按回车。现在您正在使用 bash！(恭喜)。要列出主目录中的元素，只需键入：

`$ ls /mnt/c/Users/maxou123`

(这里的 maxou123 是你的用户名).

## 7 自我测试

### 7.1 问题

#### 1.列出目录内容的命令是什么?

#### 2.终端是什么?

#### 3.给出一个众所周知的 shell 的名称。

### 答案

#### 1.列出目录内容的命令是什么?

1.ls(UNIX 系统)

2.dir(Windows 系统)

#### 2.终端是什么?

1.终端是一个程序，它提供了一个使用 shell 的接口

#### 3.给出一个众所周知的 shell 的名称。

1.bash

## 8 要点

- 图形用户界面并不总是存在
- 为了与计算机交互，我们可以使用图形界面或命令行界面。(命令行界面)
- 要使用 CLI，我们必须打开一个终端应用程序，该应用程序提供与 shell 交互的接口
- Bash 是一种 shell
- 您可以通过键入命令名称和最终选项来启动命令，然后按回车键。
- 我们将使用终端来启动特定于 go 的命令。

## 参考文献

